package main

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/tidy"
)

type User struct {
	Id       int       `json:"id"`
	Name     string    `json:"name" clean:"string,trim,strip,title"`
	Location *Location `json:"location"`
	Email    string    `json:"email" clean:"string,trim,strip"`
}

type Location struct {
	Code    string `json:"code" clean:"string"`
	City    string `json:"city" clean:"string,trim"`
	Country string `json:"country" clean:"string,trim"`
}

func main() {

	var data = []byte(`{ "id": 10, "name": "       Ricky morrison   ", "location": {"city": "Spalding", "country": "  Canada"}, "email": "dev@roderickmorrison.net     " }`)

	var user User
	err := json.Unmarshal(data, &user)
	if err != nil {
		log.Fatal(err)
	}

	user.Email = fmt.Sprintf("%c%c%s%c%c%c", '\n', '\r', user.Email, '\n', '\t', '\t')

	ind, err := json.MarshalIndent(user, "", "  ")

	fmt.Printf("Dirty user: \n%s\n", string(ind))

	if err := tidy.CleanStruct(&user); err != nil {
		log.Fatal(err)
	}

	ind, err = json.MarshalIndent(user, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Cleaned User: \n%s\n", string(ind))
}

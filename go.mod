module bitbucket.org/_metalogic_/tidy

go 1.15

require (
	bitbucket.org/_metalogic_/log v1.4.1
	bitbucket.org/_metalogic_/validator v0.0.0-20220208154447-42f0dee1da4f
)

package tidy

import (
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/_metalogic_/log"
	"bitbucket.org/_metalogic_/validator"
)

// Generic data cleaner.
type Cleaner interface {
	// Clean method performs value cleanup.
	Clean(interface{}) interface{}
}

// DefaultCleaner does nothing.
type DefaultCleaner struct {
}

func (v DefaultCleaner) Clean(val interface{}) interface{} {
	return nil
}

// StringCleaner cleans string according to parameters
type StringCleaner struct {
	Ltrim     bool
	Rtrim     bool
	Trim      bool
	Titlecase bool
	Upcase    bool
	Downcase  bool
	Strip     bool
}

func (v StringCleaner) Clean(val interface{}) interface{} {
	cleaned := val.(string)
	l := len(cleaned)

	if l == 0 {
		return val
	}

	if v.Ltrim {
		cleaned = validator.LeftTrim(cleaned, "")
	}
	if v.Rtrim {
		cleaned = validator.RightTrim(cleaned, "")
	}
	if v.Trim {
		cleaned = validator.Trim(cleaned, "")
	}
	if v.Upcase {
		cleaned = strings.ToUpper(cleaned)
	}
	if v.Downcase {
		cleaned = strings.ToLower(cleaned)
	}
	if v.Titlecase {
		cleaned = strings.Title(cleaned)
	}
	if v.Strip { // strip non-printing ASCII including newlines
		cleaned = validator.StripLow(cleaned, false)
	}
	return cleaned
}

// Returns cleaner struct corresponding to validation type
func getCleanerFromTag(tag string) Cleaner {
	args := strings.Split(tag, ",")

	switch args[0] {
	case "string":
		cleaner := StringCleaner{}
		for _, arg := range args[1:] {
			switch arg {
			case "ltrim":
				cleaner.Ltrim = true
			case "rtrim":
				cleaner.Rtrim = true
			case "trim":
				cleaner.Trim = true
			case "upper":
				cleaner.Upcase = true
			case "lower":
				cleaner.Downcase = true
			case "title":
				cleaner.Titlecase = true
			}
		}
		return cleaner
	}

	return DefaultCleaner{}
}

// Performs actual data cleanup using tag definitions on the struct
func CleanStruct(x interface{}) (err error) {

	if !structRef(x) {
		return fmt.Errorf("argument must be a reference to a struct")
	}

	v := reflect.ValueOf(x).Elem()

	if !v.CanAddr() {
		return fmt.Errorf("cannot assign to the item passed, item must be a pointer in order to assign")
	}

	for i := 0; i < v.NumField(); i++ {
		fieldVal := v.Field(i)

		if structRef(fieldVal.Interface()) {
			val := fieldVal.Interface()
			err := CleanStruct(val)
			if err != nil {
				log.Error(err)
			}
			fieldVal.Set(reflect.ValueOf(val))
			continue
		}

		// Get the field tag value
		tag := v.Type().Field(i).Tag.Get("clean")

		// Skip if tag is not defined or ignored
		if tag == "" || tag == "-" {
			continue
		}

		// Get the cleaner that corresponds to the tag
		cleaner := getCleanerFromTag(tag)
		cleaned := cleaner.Clean(fieldVal.Interface())

		if !fieldVal.CanSet() {
			return fmt.Errorf("cannot assign to %s, item must be a pointer in order to assign", fieldVal)
		}
		fieldVal.Set(reflect.ValueOf(cleaned))
	}

	return nil
}

func structRef(v interface{}) bool {
	k := reflect.ValueOf(v).Kind()
	if k != reflect.Ptr {
		return false
	}
	k = reflect.ValueOf(v).Elem().Kind()
	if k != reflect.Struct {
		return false
	}
	return true
}

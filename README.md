# tidy

## Description

tidy is a Go package that provides configurable and extensible data clean up capabilities.
It has the following features:

* can tidy data of different types, e.g., structs, strings, byte slices, slices, maps, arrays.
* can tidy custom data types as long as they implement the `Cleanable` interface.


## Getting Started

The tidy package mainly includes a set of tidy functions and two cleanup methods. You use 
tidy functions to describe how a value should be cleaned, and you call either `tidy.Clean()`
or `tidy.CleanStruct()` to clean the value.

### Installation

Run the following command to install the package:

```
go get bitbucket.org/_metalogic_/tidy
```

### Cleaning a Simple Value

TODO

### Cleaning a Struct

For a struct value, you want to cleanup its fields. For example, in a RESTful application, you
may unmarshal the request payload into a struct and then cleanup the struct fields. You can use `tidy.CleanStruct(&struct)`
to achieve this purpose. A single struct can have cleanup functions for multiple fields, and a field can be associated with cleanup 
functions. For example,

```go
	var data = []byte(`{ "id": 10, "name": "       Ricky morrison   ", "location": {"city": "Spalding", "country": "  Canada"}, "email": "dev@roderickmorrison.net     " }`)

	var user User
	err := json.Unmarshal(data, &user)
	if err != nil {
		log.Fatal(err)
	}

	user.Email = fmt.Sprintf("%c%c%s%c%c%c", '\n', '\r', user.Email, '\n', '\t', '\t')

	ind, err := json.MarshalIndent(user, "", "  ")

	fmt.Printf("Dirty user: \n%s\n", string(ind))
	...
	Dirty user: 
    {
       "id": 10,
       "name": "       Ricky morrison   ",
       "location": {
         "code": "",
         "city": "Spalding",
         "country": "  Canada"
        },
    "email": "\n\rdev@roderickmorrison.net     \n\t\t"
    }
    ...

	if err := tidy.CleanStruct(&user); err != nil {
		log.Fatal(err)
	}

	ind, err = json.MarshalIndent(user, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Cleaned User: \n%s\n", string(ind))

    fmt.Printf("%+v", user)
    Cleaned User: 
    {
       "id": 10,
       "name": "Ricky Morrison",
       "location": {
          "code": "",
          "city": "Spalding",
          "country": "Canada"
        },
        "email": "dev@roderickmorrison.net"
    }
```

Note that when calling `tidy.CleanStruct` to clean a struct, you should pass to the method a pointer 
to the struct instead of the struct itself. This implies that nested structs must be referenced if they
are to be cleaned.

## Built-in Cleanup Functions

The following tags are recognized for struct string fields:

* `trim`: trims leading and trailing white space
* `ltrim`: trims leading  white space
* `rtrim`: trims trailing white space
* `title`: converts string to title case
* `upper`: converts string to upper case
* `lower`: converts string to lower case

